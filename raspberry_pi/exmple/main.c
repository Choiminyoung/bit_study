#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <wiringPi.h>

#define LED1 		1    // led output 1
#define LED2 		10   // led output 10
#define SW_START 	5    // sw stop pin 5
#define SW_STOP 	6 	 // sw stop pin 6

#define DEBUGING 	1

// sw2를 이용 LED제어
// 2개의 프로세스 생성
// 하나의 프로세스에서 sw를 on/off 하는 상태에 따라서
// 다른 프로세스는 상태를 체크 
// 체크후 핸들러 처리.

// sigHandeller
// 
// fork()는 exit로 process를 뒷처리를 해주어야 한다.
//
//

int main(int argc, char** argv){

	int pid = 0;
	int i = 0;
		
	pid = fork();
	//fork 생성
	
	for(i = 0; i < 10; i++){
		switch(pid){
			case -1:	// fork 실패
				perror("fork실패");
				break;
			case 0:		// 자식 프로세스
				if(DEBUGING)
					printf("c pid :%d getpid : %d pid == 0\n", pid, getpid());
				sleep(0.5);
				break;
			default:	// 부모 프로세스
				if(DEBUGING)
					printf("p pid :%d getpid : %d pid == 1\n", pid, getpid());
				sleep(0.5);

				break;
		}
	}
	// p-process 일때
	// c-process 일때
	
	return 0;
}


void pin_init()
{
	pinMode(SW_START, INPUT);
	pinMode(SW_STOP, INPUT);
	pinMode(SW_START, OUTPUT);
	return ;
}
