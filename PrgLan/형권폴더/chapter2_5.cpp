/*

	소개한 버블정렬은 오름차순으로 정렬하는 알고리즘이다. 이를 수정하여서 내림차순으로 정렬하는 알고리즘을 구현하여라.

*/

#include <stdio.h>

void DesSort(int ary[], int len);
void Swap(int *a, int *b);

int main()
{
	int arr[7] = { 3,2,4,1 };
	int i,arr_len;

	for (i = 0; i < 7; i++)
	{
		
		printf("입력: ");
		scanf("%d", &arr[i]);

	}

	arr_len = sizeof(arr) / sizeof(int);
	DesSort(arr, arr_len);

	for (i = 0; i < arr_len ; i++)
	{
	
		printf("%d ", arr[i]);

	}

	printf("\n");

	return 0;

}

void DesSort(int ary[], int len) {
	int i, j;


	for (i = 0; i < len - 1; i++)
	{
		for (j = 0; j < (len - i) - 1; j++)
		{
			if (ary[j] < ary[j + 1])
			{

				Swap(&ary[j], &ary[j + 1]);
				
			}
		}
	}
}

void Swap(int *a, int *b)
{
	int temp;

	temp = *a;
	*a = *b;
	*b = temp;
}