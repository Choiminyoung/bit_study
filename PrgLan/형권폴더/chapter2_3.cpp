/*

	길이가 10인 배열을 선언하고 총10개의 정수를 입력 받는다. 단, 입력 받은 숫자가 홀수이면 배열의 앞에서부터 채워나가고, 짝수이면 뒤에서부터 채워나가는 형식을 취하기로 하자.
	따라서 사용자가 [1,2,3,4,5,6,7,8,9,10]을 입력햇다면, 배열에는 [1,3,5,7,9,10,8,6,4,2,]의 순으로 저장이 되어야 한다.

*/

#include <stdio.h>

void saveArr(int *arr, int num,int * odd_index,int * even_index);

int main() {

	int arr[10];
	int i,num,arr_len;
	int odd_index, even_index;
	
	arr_len = sizeof(arr) / sizeof(int);
	odd_index = 0, even_index = arr_len - 1;

	printf("총 10개의 숫자 입력\n");

	for (i = 0; i < 10; i++) {

		printf("입력: ");
		scanf("%d", &num);
		saveArr(arr, num,&odd_index,&even_index);
	}

	printf("배열 요소의 출력 :");
	for (i = 0; i < arr_len; i++)
	{

		printf("%2d ", arr[i]);

	}
	
	printf("\n");
	return 0;
}

void saveArr(int * arr, int num, int *odd_index, int *even_index)
{

	if (num % 2 == 1)
	{
		arr[*odd_index] = num;
		(*odd_index)++;
	}
	else 
	{
		arr[*even_index] = num;
		(*even_index)--;
	}

	return;

}