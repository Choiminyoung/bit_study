/*

	회문은 앞으로 읽으나 뒤로 읽으나 차이가 없는 단어들을 뜻한다. 예를 들어서 level, bob과 같은 단어들은 회문에 속한다.
	이에 우리는 인자로 전달되는 영단어가 회문인지 아닌지를 판단해서 그 결과를 출력하는 기능의 함수를 정의하고 이에 적절한 main 함수를 정의해 보고자 한다.
	단 구현의 편의를 위해서 대소문자까지 일치해야 회문으로 인정하기로 하겠다(이는 어디까지나 구현의 편의를 고려한 제약사항일 뿐이다.)

*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>


bool checkPalindrome(char * str, int str_len);
int main() 
{

	char *str = (char *)malloc(sizeof(char) * 100);
	int str_len;
	bool flag=0;
	
	printf("문자열을 입력: ");
	scanf("%s", str);

	str_len = strlen(str);

	flag = checkPalindrome(str, str_len);

	if (flag == 0)
		printf("회문이 아닙니다.\n");
	else
		printf("회문 입니다.\n");


	free(str);
	return 0;

}

bool checkPalindrome(char *str, int str_len) 
{
	int i = 0, j = str_len-1;

	if (str_len % 2 != 0){
		while (1)
		{

			
			if (str[i] != str[j])
				return false;
			if (i == j)break;
			i++;
			j--;

		}
	}
	else 
	{
		while (1) 
		{

			if (str[i] != str[j])
				return false;
			if (i > j)break;
			i++;
			j--;

		}
	}

	return true;

}