/*
	
	프로그램 사용자로부터 10진수 형태로 정수를 하나 입력 받은 다음, 이를 2진수로 변환해서 출력하는 프로그램을 작성해 보자.

*/

#include <stdio.h>
#include <stdlib.h>

void decTobin(int dec_num,int arr_len);
int decAndlen(int dec_num);

int main()
{
	int dec_num;
	int len;

	printf("10진수 정수 입력: ");
	scanf("%d", &dec_num);

	len = decAndlen(dec_num);
	decTobin(dec_num,len);

	return 0;

}

int decAndlen(int dec_num) 
{
	int i;

	for ( i = 0;; i++) 
	{

		if (dec_num == 0)
			break;
		dec_num /= 2;

	}

	return i;
}

void decTobin(int dec_num,int arr_len) 
{
	int i;
	int *arr = (int *)malloc(sizeof(int)*arr_len);

	for (i = arr_len; i != 0; i--) 
	{

		arr[i - 1] = dec_num % 2;
		dec_num /= 2;

	}

	for (i = 0; i < arr_len; i++)
	{

		printf("%d", arr[i]);

	}

	printf("\n");

	free(arr);
	return;
}