/*

	길이가 10인 배열을 선언하고 총 10개의 정수를 입력 받아서, 홀수와 짝수를 구분 지어 출력하는 프로그램을 작성해 보다. 일단 홀수부터 출력하고 나서 짝수를 출력하도록 하자.
	단,10개의 정수는 main 함수 내에서 입력 받도록 하고,
	배열 내에 존재하는 홀수만 출력하는 함수와 배열내에 존재하는 짝수만 출력하는 함수를 각각 정의해서 이 두 함수를 호출하는 방식으로 프로그램을 완성하자.

*/

#include <stdio.h>

void printOddArray(int *arr,int arr_len);
void printEvenArray(int *arr,int arr_len);
#define MAX_ARRAY_LENGTH 10

int main()
{
	int arr[MAX_ARRAY_LENGTH];
	int i = 0,arr_len;

	printf("총 10개의 숫자 입력\n");

	for (i = 0; i < MAX_ARRAY_LENGTH; i++)
	{
	
		printf("입력 : ");
		scanf("%d", &arr[i]);
	
	}

	arr_len = sizeof(arr) / sizeof(int);

	printOddArray(arr,arr_len);
	printEvenArray(arr, arr_len);

	return 0;

}

void printOddArray(int *arr, int arr_len) 
{
	int i = 0;

	printf("홀수 출력: ");

	for (i = 0; i < arr_len; i++)
	{
		if(arr[i]%2==1)
			printf("%2d ", arr[i]);
		if (i < (arr_len-1) && arr[i + 1] % 2 == 1)
			printf(",");
	}

	printf("\n");

	return;
}

void printEvenArray(int *arr, int arr_len) 
{
	int i = 0;

	printf("짝수 출력: ");
	
	for (i = 0; i < arr_len; i++)
	{

		if (arr[i] % 2 == 0)
			printf("%2d ", arr[i]);
		if (i!=0 && i < (arr_len - 1) && arr[i + 1] % 2 == 0)
			printf(",");

	}

	printf("\n");

	return;
}