#include <stdio.h>
#include "./main.h"
#include <stdlib.h>
#include <string.h>

int main()
{
	int A, B;
	int result;
	double result_f;

	A=30,B=15;

	result = add_user(A,B);
	printf("%d + %d : %d\n",A,B,result);
	result = sub_user(A,B);
	printf("%d - %d : %d\n",A,B,result);
	result = mul_user(A,B);
	printf("%d * %d : %d\n",A,B,result);
	result_f = div_user(A,B);
	printf("%d / %d : %d\n",A,B,result);

	return 0;
}


