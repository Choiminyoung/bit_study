set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim

" vim에서 플러그인 설치시 추가할부분 - 시작 call vundle#begin()"
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'Valloric/YouCompleteMe'
Plugin 'airblade/vim-gitgutter'
Plugin 'VundleVim/Vundle.vim'
Plugin 'vim-airline/vim-airline'
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/syntastic'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'kchmck/vim-coffee-script'

call vundle#end()            " required
"끝 부분
filetype plugin indent on    " required

let g:ctrlp_custom_ignore = {
  \ 'dir':  '\.git$\|public$\|log$\|tmp$\|vendor$',
  \ 'file': '\v\.(exe|so|dll)$'
  \ }


set hlsearch " 검색어 하이라이팅
set nu " 줄번호
set autoindent " 자동 들여쓰기
set scrolloff=2
set wildmode=longest,list
set ts=4 "tag select
set sts=4 "st select
set sw=1 " 스크롤바 너비
set autowrite " 다른 파일로 넘어갈 때 자동 저장
set autoread " 작업 중인 파일 외부에서 변경됬을 경우 자동으로 불러옴
set cindent " C언어 자동 들여쓰기
set bs=eol,start,indent
set history=256
set laststatus=2 " 상태바 표시 항상
"set paste " 붙여넣기 계단현상 없애기
set shiftwidth=4 " 자동 들여쓰기 너비 설정
set showmatch " 일치하는 괄호 하이라이팅
set smartcase " 검색시 대소문자 구별
set smarttab
set smartindent
set softtabstop=4
set tabstop=4
set ruler " 현재 커서 위치 표시
set incsearch
set statusline=\ %<%l:%v\ [%P]%=%a\ %h%m%r\ %F\

" ctags 적용
set tags=/usr/include/tags 


" 마지막으로 수정된 곳에 커서를 위치함
au BufReadPost *
\ if line("'\"") > 0 && line("'\"") <= line("$") |
\ exe "norm g`\"" |
\ endif

" 파일 인코딩을 한국어로
if $LANG[0]=='k' && $LANG[1]=='o'
"set fileencoding=korea
	set fileencoding=utf-8
endif

" 구문 강조 사용
if has("syntax")
 syntax on
 endif

function InsertTabWrapper()
	let col = col('.') - 1
	if !col || getline('.')[col - 1] !~ '\k'
		return "\<tab>"
	else
		return "\<c-p>"
	 endif
	 endfunction
  
inoremap <tab> <c-r>=InsertTabWrapper()<cr>

" 컬러 스킴 사용
 "colorscheme jellybeans
 

 let g:ycm_global_ycm_extra_conf = '~/.vim/.ycm_extra_conf.py' 
 let g:ycm_confirm_extra_conf = 0 
 let g:ycm_key_list_select_completion = ['', ''] 
 let g:ycm_key_list_previous_completion = ['', ''] 
 let g:ycm_autoclose_preview_window_after_completion = 1 
 let g:ycm_warning_symbol = '>*' 
 nnoremap g :YcmCompleter GoTo 
 " nnoremap gg :YcmCompleter GoToImprecise 
 " nnoremap d :YcmCompleter GoToDeclaration 
 " nnoremap t :YcmCompleter GetType 
 " nnoremap p :YcmCompleter GetParent 
 

