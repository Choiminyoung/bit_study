#include <stdio.h>

#define LOOPSIZE 4

char* studentName[4] = { 
	"철희", "철수" ,"영희", "영수"
};

char* subjectName[4]={
	"국어","영어","수학","국사"
};

typedef struct {
	char* name;
	char* subject[4]; 
	int score[4];
	int total;
}stuInfo;

typedef struct {
	stuInfo* stuSub;
}strSub;

void storeScore(stuInfo* stu);
void storeName(stuInfo* stu);
void scorePrint(stuInfo* stu);
void storeStruct();
void storeSubject(stuInfo* stu);
void subjectTotal(stuInfo* stu);

int main()
{
	storeStruct();
	// 학생 구조체에 과목 점수 저장 및 출력
	
	return 0;
}


void storeStruct()
{
	stuInfo student[4];
	// 학생의 성적을 저장하는 구조체 4개 선언
	strSub* sub;

	sub = student;
	
	storeSubject(student);
	// 구조체에 과목 저장

	storeName(student);
	// 구조체에 학생이름을 저장

	storeScore(student);
	// 성적을 저장하는 내용

	subjectTotal(student);
	// 과목 점수 총합을 구함

	scorePrint(student);
	// 저장된 점수 출력  
}

/*
 * 구조체에 학생 이름 저장하는 기능
 */
void storeName(stuInfo* stu)
{
	int i = 0;
	
	for(i = 0; i < LOOPSIZE; i++)
	{
		stu[i].name = studentName[i];
		// 학생의 이름을 저장
	}
}

/*
 * 구조체에 과목명을 저장하는 기능
 */
void storeSubject(stuInfo * stu)
{
	int i = 0,j=0;
	
	for(i = 0; i < LOOPSIZE; i++)
	{
		for(j = 0; j < LOOPSIZE; j++)
		{
			stu[i].subject[j] = subjectName[j];
			// 구조체에 과목명을 저장
		}
	}

	return;
}

/*
 * 학생 점수 저장
 */
void storeScore(stuInfo *stu)
{
	int i = 0, j = 0;

	/*
	 * 점수를 저장
	 */

	do{
		for(j = 0; j < LOOPSIZE; j++)
		{
			printf("%s 학생의 %s 점수를 입력하세요.", stu[i].name, stu[i].subject[j]);
			scanf("%d",&stu[i].score[j]);
			// 과목에 해당하는 점수를 입력받음
		}
		i++;		puts("");
	} while(i < LOOPSIZE);// 초기에는 문제가 없었으나 기능 추가시 NULL이 잡히지 않아서 문제가 발생.
	// 학생이름이 NULL이 아닐경우 == 학생 이름이 있는경우
	// 한 점수를 입력 받기위해 != NULL 조건을 사용해봄

	puts("");

	return;
}

/*
 * 저장된 점수 출력
 */
void scorePrint(stuInfo *stu)
{
	int i,j = 0;

	for(i=0; i < LOOPSIZE; i++)
	{
		printf("이름 : %s \n",stu[i].name);

		for(j=0; j < LOOPSIZE; j++)
		{
			printf("과목명 : %s ", stu[i].subject[j]);
			printf("점수 : %d \n", stu[i].score[j]);
		}
			printf("total : %d \n", stu[i].total);
		puts("");
	}
}


/*
 * 전체 과목의 합을 구하는 기능
 */
void subjectTotal(stuInfo* stu)
{
	int total=0, i, j = 0;

	for(i = 0; i < LOOPSIZE; i++)
	{
		for(j = 0; j < LOOPSIZE; j++)
		{
			total += stu[i].score[j];
			// 과목 점수 누적
		}

		stu[i].total = total; // 구조체 변수는 초기화를 못하기에 필수로.
	}
}
