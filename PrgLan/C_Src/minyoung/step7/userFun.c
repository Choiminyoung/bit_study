void func1() 
{
	int arr[5] = {1,2,3,4,5}, i= 0;
	int* ptr = arr;

	for(i=0; i<5; i++)
	{
		ptr = &arr[i];
		*ptr += 2;
		printf("%d ",arr[i]);
	}
	puts("");
}


void func2()
{
	int arr[5] = {1,2,3,4,5}, i= 0;
	int* ptr = arr;

	for(i =0; i<5; i++){
		*(ptr + i) += 2;
	}

	for(i=0; i<5; i++)
	{
		printf("%d ",arr[i]);
	}
	puts("");
	
	return;
}

void func3()
{
	int arr[5]={1,2,3,4,5},i=0;
	int* ptr = &arr[4];
	int buf = 0;

	for(i=4; i>=0; i--)
	{
		buf += *(ptr-i);
		*(ptr-i) -= 2;

		printf("&a:%p  &p: %p\n",&arr[i], &ptr[i]);
		printf("주소:%p\n 카운트:%d \n 저장값:%d \n-----------\n", &ptr[i],i,*(ptr-i));

	}

	for(i =4; i>=0; i--)
	{
		printf("%d ",*(ptr-i));
	}


	printf("buf : %d \n",buf);
	
	return;
}


void func4()
{
	int arr[6] = {1,2,3,4,5,6}, i=0;
	int size = sizeof(arr)/sizeof(int);
	int* pback = &arr[5]; 
	int* pfron = arr; 
	int buf;


	for(i=0; i < size/2; i++)
	{
		buf = *(pfron);

		*pfron = *pback;

		*(pback) = buf;
		pfron++;
		pback--;
	}

	pfron = arr;

	for(i=0; i<6; i++)
		printf(" %d ",*(pfron+i));
	return;
}

