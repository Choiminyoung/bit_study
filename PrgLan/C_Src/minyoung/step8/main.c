#include <stdio.h>
#include <fcntl.h>

// 코딩도장 예제

/*
 * 24.4 비트 연산자로 플래그 처리하기(-)
 * 26.3 case에서 break 생략 응용하기(-)
 * 31.7 심사문제: 산 모양으로 별 출력하기 퀴즈
 * 35.2 메모리에 값 저장하기
 * 37.4 2차원 배열의 크기 구하기
 * 38.3 포인터에 할당된 메모리를 2차원 배열처럼 사용하기
 * 41.5 연습문제: 문자열 비교하기
 * 49.3 구조체 포인터에 구조체 변수의 주소 할당하기
 * 56.1 구조체 비트 필드를 만들고 사용하기
 * 70.4 파일에서 문자열 읽기
 * 72.2 파일에서 구조체 읽기
 */

void buildStar();

#define BUILDSTAR_SIZE 10
#define BUFMAX 1024

int main(int argc, char** argv)
{


	int n, in, out;
	char buf[1024];

	if(argc < 3)
	{
		write(2,"usage : copy file 1 file2\n",25);
		return -1;
	}

	printf("argc = %d \n",argc);
	printf("argv[0] = %s \n",argv[0]);
	printf("argv[1] = %s \n",argv[1]);
	printf("argv[2] = %s \n",argv[2]);
	
	if((in = open(argv[1],O_RDONLY)) < 0)
	{
		perror(argv[1]);
		return -1;
	}

	if((out = open(argv[2], O_WRONLY | O_CREAT | O_TRUNC | S_IRUSR | S_IWUSR)) < 0)
	{
		perror(argv[2]);
		return -1;
	}

	while((n=read(in,buf,sizeof(buf))) > 0)
		write(out,buf,n);
	return 0;
}



// 산 모양으로 별 출력하기
void buildStar()
{
	int i = 0, j = 0 ;

	for(i =0; i < BUILDSTAR_SIZE; i++){
		for(j = 0; j < i; j++ )
		{
			if(j)
				printf("-");
			printf("*");
		}
		puts(" ");
	}
	return;
}
