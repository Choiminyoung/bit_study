#include <stdio.h>


void one_for();
void two_for();
void input_multiplication_table();

int main()
{

	puts("구구단");
	
	one_for();
	two_for();
	input_multiplication_table();
	
	return 0;
}




void one_for(){
	int i,j = 0;

	puts("==================================== one for ====================================");

	for(i = 1; i < 9; i++){
		printf(" %2d * %2d = %2d", 	 i, i+1, (i+1)*i); 
		printf(" %2d * %2d = %2d", 	 i, i+2, (i+2)*i); 
		printf(" %2d * %2d = %2d\n", i, i+3, (i+3)*i);  
	}
	puts("==================================== one for ====================================");
}

void two_for()
{
	int i,j = 0;

	puts("=============== two for===============");
	for(i = 1; i < 9; i++)
	{
		for(j = 1; j < 9; j++)
		{
			printf(" %2d * %2d = %2d ",   j, i+1, (i+1)*j); 
			printf(" %2d * %2d = %2d ",   j, i+2, (i+2)*j); 
			printf(" %2d * %2d = %2d\n",  j, i+3, (i+3)*j);  
		}
		puts("--------------------------------------------");
	}
	puts("=============== two for===============");
}

void input_multiplication_table()
{
	int input, i, multiPlusOne, multiPlusTwo;

	puts("확인하고자 하는 구구단을 입력");
	scanf("%d",&input);
	if(input > 9){
		printf("9단까지만 진행\n"); 
		input = 9;
	}

	multiPlusOne = input+1;
	multiPlusTwo = input+2;


	if(multiPlusOne > 9){
		multiPlusOne = 9;
		printf("입력값+1 : 최대값은 9를 넘을수 없음\n");
	} 
	if(multiPlusTwo > 9) {
		multiPlusTwo = 9;
		printf("입력값+2 : 최대값은 9를 넘을수 없음\n");
	}
	for(i = 0; i < 9; i++)
	{

		printf(" %2d * %2d = %2d ",   input  , i, i*input); 
		printf(" %2d * %2d = %2d ",   multiPlusOne, i, i*(multiPlusOne)); 
		printf(" %2d * %2d = %2d\n",  multiPlusTwo, i, i*(multiPlusTwo));  
	}
	puts("--------------------------------------------");
	return ;
} 
