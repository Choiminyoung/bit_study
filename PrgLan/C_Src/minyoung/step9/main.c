#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

// 파일 포인터 관련 예제 

#define _CRT_SECURE_NO_WARNINGS

void c_file();
int l_file(char** argv);

char buf[200];

int main(int argc, char** argv)
{
	int fd, i;
	int status, mode;
//	char* temp = "test";
	
	fd = open(argv[1], O_RDONLY);	//읽기, 쓰기 모두 가능하게 연다.

	if(fd < 0){
		perror("open error\n");
		exit(1);
	}

	if(read(fd,buf,sizeof(buf)) < 0){
		perror("open faile");
		exit(2);
	} else {
		puts("open sucess");
		puts(buf);
		close(fd);
	} 

	puts("for");
	for(i = 0; buf[i] != NULL; i++){
		printf("%c",buf[i]);
	}
	puts("");


	fd = open(argv[1],O_WRONLY);

	if(fd < 0){
		perror("open faile");
		exit(1);
	}

	// 데이터 쓰기
	if(write(fd,buf,sizeof(buf)) == -1){
		perror("write() Error");
		exit(0);
	}
	printf("문자열 입력\n");
	scanf("%s",buf);
	buf[strlen(buf)] = '\n';
		

	// 아래 주석은 파일디스크립터 설정 모드를 확인하기 위한 코드
	/*
	status = fcntl(fd,F_GETFL,0);
	mode = status & O_ACCMODE;

	if(fd < 0){
		perror("open faile");
		exit(0);
	} else {
		printf("fd : %d \n",fd);
		if(write(fd,temp,strlen(temp)) > 0)
			close(fd);
		else{
			if(mode == O_RDONLY){
				printf("O_RDONLY setting\n");
				printf("mode : %d\n",mode);
			}
			else if (mode == O_WRONLY)
				printf("O_WRONLY setting\n");
			else if (mode == O_RDWR)
				printf("O_RDWR setting\n");


			printf("status : %x\n",status);
			perror("write()");
		}
	}
	*/

	return 0;
}




int l_file(char **argv)
{
}

void c_file(void )
{
	
	char s1[10];
	int num1;

	FILE* fp = fopen("hello.txt","r");	// hello.txt 파일을 읽기 모드로 열기

	fscanf(fp,"%s %d",s1,&num1);
	printf("%s %d\n",s1,num1);

	fclose(fp);

	return ;
}
