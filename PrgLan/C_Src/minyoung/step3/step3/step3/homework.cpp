﻿#include <stdio.h>

/*
도전 1.

길이가 10인 배열을 선언하고 총 10개의 정수를 입력받아서,
홀수와 짝수를 구분 지어 출력하는 프로그램을 작성해 보자
일단, 홀수부터 출력하고 나서 짝수를 출력하도록하자
단, 10개의 정수는 main함수 내에서 입력 받도록 하고, 
배열 내에 존재하는 "홀수"만 출력하는 함수와 
배열 내에 존재하는 "짝수"만 출력하는 함수를 각각 정의해서
이 두 함수를 호출하는 방식으로 프로그램을 완성해보자

도전 2.

프로그램 사용자로부터 10진수 형태로 정수를 하나 입력받은 다음,
이를 2진수로 변환해서 출력하는 프로그램을 작성해보자

도전 3.

길이가 10인 배열을 선언하고 총 10개의 정수를 입력 받는다.
단, 입력받은 숫자가 홀수이면 배열의 앞에서부터 채워 나가고
짝수이면 뒤에서 부터 채워나가는 형식을 취하기로 하자
따라서, 사용자가[1,2,3,4,5,6,7,8,9,10]을 입력했다면 
배열에는 [1,3,5,7,9,10,8,6,4,2]의 순으로 저장 되어야 한다.

도전 4. 

회문(Palindrome)은 앞으로 읽으나 뒤로 읽으나 차이가 없는 단어들을 뜻한다.
예를 들어서 level
입력한 단어가 회문인지 아닌지 판단해서 그 결과를 출력하는 기능의 함수.
단, 편의를 위해 대소문자 구분은 하지 않는다.

guide:
문자열의 길이를 알아야 회문인지 아닌지를 판단할 수 있다.
문자열의 길이를 계산해서 반환하는 표준함수는 string.h에 정의되어 있다.

도전 5.

내림 차순의 버블정렬을 구현하시오.
*/

//	--------------------------------------------------- 문제 1 시작---------------------------------------------------

/*
도전 1.

길이가 10인 배열을 선언하고 총 10개의 정수를 입력받아서,
홀수와 짝수를 구분 지어 출력하는 프로그램을 작성해 보자
일단, 홀수부터 출력하고 나서 짝수를 출력하도록하자
단, 10개의 정수는 main함수 내에서 입력 받도록 하고, 
배열 내에 존재하는 "홀수"만 출력하는 함수와 
배열 내에 존재하는 "짝수"만 출력하는 함수를 각각 정의해서
이 두 함수를 호출하는 방식으로 프로그램을 완성해보자
*/

#define MAX_INPUT_SIZE	10

void get_int_val(int* get_buf);
void odd_even_fun_main();
void find_even_odd(int*);

int main(){

	odd_even_fun_main();

	return 0; 
}

/*	도전문제 1의 main	*/ 
void odd_even_fun_main()
{	
	int sort_arr[MAX_INPUT_SIZE]={0,};
	
	get_int_val(sort_arr);
	// 10개 정수를 입력받음
	
	find_even_odd(sort_arr);
	// 홀,짝을 분류

	puts("\nmain======");
	for(i = 0 ; i< MAX_INPUT_SIZE; i++){
		printf("%d ", sort_arr[i]);
	}
	
	return ;
}

/*	10개의 정수를 입력받는 함수	*/ 
void get_int_val(int* get_buf)
{
	int i = 0;

	puts("10개의 정수를 입력하시오");

	for(i = 0; i < MAX_INPUT_SIZE; i++){
		scanf("%d",get_buf+i);
	}

	for(i = 0 ; i< MAX_INPUT_SIZE; i++){
		printf("%d ", *(get_buf+i));
	}
} 

/*	짝,홀수를 찾는 함수	*/
void find_even_odd(int* get_number, int* saveOdd, int* saveEven)
{
	int i = 0, ei = 0, oi = 0; 

	for(i = 0; i <= MAX_INPUT_SIZE; i++)
	{
		if(get_number[i] % 2 == 0)
		// 조건이 참인 경우 짝수 
		{
			
			*(saveOdd + oi) = get_number[i];
			oi++;
		}
		else
		// 조건이 거짓인 경우 홀수 
		{
			
			*(saveEven + ei) = get_number[i];
			ei++;
		}
	}

	oi = 0,ei = 0;

	return ;
}

