﻿#include <stdio.h>

/*
도전 3
사용자로부터 초(second)를 입력 받은 후에, 이를[시,분,초]의 형태로 출력하는 프로그램을 작성해보자
예) 초(second) 입력 : 3615
[h:1,m:0,s:15]

도전 6
두 개의 정수를 입력 받아서 최대 공약수(GCM)를 구하는 프로그램을 작성해 보자
예)
두 개의 정수 입력 : 6 9
두 수의 최대공약수 : 3

정수num1과num2의 최대 공약수는 num1이나 num2보다 클 수 없다. 
그리고 num1과 num2의 최대 공약수로 num1또는 num2를 나누면 나머지가 0이 된다.
이 두가지 특성을 이용하면 최대공약수를 쉽게 구할수 있다.
참고로 여유가 된다면 인터넷에서 '유클리드 호제법' 찾아보고 이를 기반으로 
문제 해결을 한번더 시도하기 바란다.

조건 정리
1. 정수 (type : int)
2. 최대 공약수는 두 수(num1이나 num2)보다 클 수 없다.(num1 || num2)
3. 최대 공약수로 num1 또는 num2를 나누면 나머지가 0이 된다.
*/

// 함수 선언

// 도전문제 3
void time_div_main();
void time_div(int sec);
int get_sec();

// 도전문제 6
void subject_switch();
void gcm_mainfun();	
int get_flag(int num1, int num2);	
void find_cm(int num1, int num2, int flag_stop);

int main()
{
	subject_switch();

	return 0;	
}

//	함수 정의부

void subject_switch(){
	int qnum = 0;

	puts("확인할 도전문제의 문제 번호 입력 3,6");
	scanf("%d",&qnum);

	switch(qnum)
	{
		case 3:
			time_div_main();
			break;
		case 6:
			gcm_mainfun();
			break;
		default:
			puts("없는 번호");
			return 0;
			break;
	}
}

void gcm_mainfun(){
	
	int num1,num2 = 0,
		flag_stop = 0;	// 최대공약수의 한계치.
	int cm_buf = 0;
		
	puts("최대 공약수를 구할 두수를 입력하시오.");	
	scanf("%d %d",&num1,&num2);	

	// 최대공약수는 두 수(num1이나 num2)보다 클 수 없다.(num1 || num2)
	flag_stop = get_flag(num1, num2);
	// 최대공약수로 num1 또는 num2를 나누면 나머지가 0이 된다.	
	find_cm(num1, num2, flag_stop);
	
	return ;
}

// 최대공약수로 num1 또는 num2를 나누면 나머지가 0이 된다.
void find_cm(int num1, int num2, int flag)
{
	int i = 1;
	int temp = 0;

	puts("공약수 찾기 시작");

	for(i = 1; i <= flag; i++){
		if(num1 % i == 0){
			if(num2 % i == 0){
				temp = i;
				printf("공약수 : %d\n",temp);
			} 
		}									
	}
	printf("최대 공약수 : %d\n",temp);
}

// 최대공약수는 두 수(num1이나 num2)보다 클 수 없다.(num1 || num2)
int get_flag(int num1,int num2)
{
	int result;
	
	if(num1 > num2){		
		result = num2;
	} else {
		result = num1;
	}
	
	return result;
}

// 사용자로부터 입력 받은 초(sec)를 시,분,초로 변환하는 함수 - main
void time_div_main()
{
	int sec = 0;

	sec = get_sec();
	time_div(sec);

	return ;
}
// 사용자로 부터 초를 입력 받는다 - sub
int get_sec()
{
	int sec = 0;

	puts("시,분,초로 변환할 초를 입력하시오");
	scanf("%d",&sec);

	return sec;
}
// 입력받은 초를 변환 - sub
void time_div(int get_sec)
{
	int  hour = 0, min = 0, buf = 0, sec = 0;

	min = get_sec / 60;
	sec = get_sec % 60;	
	hour = min / 60;
	min = min%60;	// 시간을 제외한 순수 분을 저장
	
	printf("입력초 : %d초 변환결과\n %d시 %d분 %d초\n",get_sec,hour,min,sec); 

	return; 
}