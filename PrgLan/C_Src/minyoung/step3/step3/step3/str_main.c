﻿#include <stdio.h>

int main()
{
	/*
	char str1[] = "My String";		//	변수 형태의 문자열
	char * str2 = "Your String";	//	상수 형태의 문자열

	printf("%s %s\n",str1, str2);	

	str2 = "Our String";		//	가리키는 대상 변경
	printf("%s %s\n",str1,str2);

	str1[0] = 'x';	//	문자열 변경 성공
	str2[0] = 'x';	//	문자열 변경 실패
	printf("%s %s\n",str1,str2);
	*/
//	int num1=10,num2=20,num3=30;

/*
int num1[3]={1,2,3};
	int* arr[3] = {&num1[0],&num1[1],&num1[2]};

	printf("%d \n",*arr[0]);
	printf("%d \n",*arr[1]);
	printf("%d \n",*arr[2]);
*/	
	char* strArr[3] = {"Simple","String","Array"};
	char* srr = {"String"};

	strArr[2] = strArr[1];
	strArr[2] = "String";

	printf("%s \n",strArr[0]);
	printf("%s \n",strArr[1]);
	printf("%s \n",strArr[2]);
	return 0;
}