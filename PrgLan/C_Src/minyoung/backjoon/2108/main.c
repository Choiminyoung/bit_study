#include <stdio.h>
#include <string.h>
#include <stdlib.h>
/*
 * 문제
 *		수를 처리하는 것은 통계학에서 상당히 중요한 일이다.
 *		통계학에서 N개의 수를 대표하는 기본 통계값에는 다음과 같은 것들이 있다. 
 *		단, N은 홀수라고 가정하자.
 *
 * 산술평균 : N개의 수들의 합을 N으로 나눈 값
 * 중 앙 값 : N개의 수들을 증가하는 순서로 나열했을 경우 그 중앙에 위치하는 값
 * 최 빈 값 : N개의 수들 중 가장 많이 나타나는 값:
 * 범    위 : N개의 수들 중 최대값과 최소값의 차이
 * N개의 수가 주어졌을 때, 네 가지 기본 통계값을 구하는 프로그램을 작성하시오.
 *
 * 입력
 *		첫째 줄에 수의 개수 N(1≤N≤500,000)이 주어진다. 그 다음 N개의 줄에는 정수들이 주어진다. 입력되는 정수의 절대값은 4,000을 넘지 않는다.
 *
 * 출력
 *		첫째 줄에는 산술평균을 출력한다. 소수점 이하 첫째 자리에서 반올림한 값을 출력한다.
 *		둘째 줄에는 중앙값을 출력한다.
 *		셋째 줄에는 최빈값을 출력한다. 여러 개 있을 때에는 최빈값 중 두 번째로 작은 값을 출력한다.
 *		넷째 줄에는 범위를 출력한다.
 */

#define DBUG 1

void quickSort(int left, int right, int* data);
void swap(int* a, int* b);

int main(int argc,char** argv)
{
	int N = 0, i = 0, temp = 0;
	int *parr = NULL;
	int mid;

	scanf("%d",&N);
	//while(getchar() == '\n') ;

	if((N % 2) == 0){
		puts("N == odd num");
		return -1;
	}

	parr = (int*)malloc(N * sizeof(int));

	
	if(DBUG)
		puts("==================================== input number ====================================");
	for(i = 0; i< N; i++){
		scanf("%d",&(parr[i]));
	}

	mid = (N / 2) + 1;


	for(i = 0; i< N; i++){
		temp += parr[i];
	}

	printf("중앙값 : %d 총합 : (total sum) : %d\n", mid, temp);
	printf("입력갯수 : %d 산술평균 : (total sum / N) : %d\n", N, temp/N);

	quickSort(0,N-1,parr);

	for(i = 0; i < N;i++){
		printf("%d ",parr[i]);
	}
	puts("");

	free(parr);
	return 0;
}


void quickSort(int left, int right, int* data){
	int pivot = left;
	int j = pivot, i  = left;
	
	if(left < right){
		for(;i <= right;i++){
			if(data[i] < data[pivot]){
				j++;
				swap(&data[j], &data[i]);
			}
		}
		swap(&data[left], &data[j]);
		pivot = j;

		quickSort(left, pivot-1, data);
		quickSort(pivot+1, right, data);
	}
	return ;
}

void swap(int* a, int* b)
{
	int t = *a;
	*a = *b;
	*b = t;
	return ;
}
