#include <stdio.h>

#define MAX_VALUE 9

void gugudan(int dan, int num);


int main()
{
	int dan = 0, num = 1;
		
	printf("몇 단?");
	scanf("%d",&dan);
	//fflush(stdin);

	do
	{
		if(dan > 9 )
		{
			printf("현재 입력 값 : %d은 1 ~ 9 사이 값을 입력하여야 합니다.",dan);
			printf("몇 단?");
			scanf("%d",&dan);
			fflush(stdin);

		}
		else if(dan < 0)
		{
			printf("현재 입력 값 : %d은 1 ~ 9 사이 값을 입력하여야 합니다.",dan);
			printf("몇 단?");
			scanf("%d",&dan);
			fflush(stdin);
		}
	}while(dan < 0 || dan > MAX_VALUE);

	gugudan(dan, num);

	return 0;
}


void gugudan(int dan, int num)
{
	while(num < 10)
	{

		printf("%d * %d = %d \n",dan, num, dan*num);

		num++;
	}
	puts("");
	return ;
}
