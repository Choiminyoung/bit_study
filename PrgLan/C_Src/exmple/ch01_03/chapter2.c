#include <stdio.h>

void printfOne(void)
{
	printf("Hello Everybody\n");
	printf("%d\n", 1234);
	printf("%d %d\n", 10, 20);
	return 0;
}

void printfTwo(void)
{
	printf("My age: %d \n", 20);
	printf("%d is my point \n", 100);
	printf("Good \nmorning \neverybody\n");	
	return 0;
}


int main(void)
{
	printfOne();
	printfTwo();
}
