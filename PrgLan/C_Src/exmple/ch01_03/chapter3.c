#include <stdio.h>

// 함수 원형 선언  
void addOne(void);
void varDeclAndInit(void);
void addTwo(void);
void operatorOne(void);
void operatorTwo(void);
void operatorThree(void);
void operatorFour(void);
void operatorFive(void);
void operatorSix(void);
void operatorSeven(void);
void commaOp(void);
void addThree(void);


int main(void)
{
	addOne();
	varDeclAndInit();
	addTwo();
	addThree();
	operatorOne();
	operatorTwo();
	operatorThree();
	operatorFour();
	operatorFive();
	operatorSix();
	operatorSeven();
	addThree();
	
	return 0;
	
}


//=============================================
// 덧셈 프로그램의 구현에 필요한 + 연산자 (P.3)
//=============================================
void addOne(void)
{
	// + 연산자의 사용 
	3+4;
}


//=============================================
//변수의 다양한 선언 및 초기화 방법 (P.5)
//=============================================
void varDeclAndInit(void)
{ 
	int num1, num2,	num3=30,num4=40;
	// 변수의 선언과 동기에 초기화
 

	printf("num1: %d, num2: %d \n", num1, num2);
	num1=10;
	num2=20;

	printf("num1: %d, num2: %d \n", num1, num2);	
	printf("num3: %d, num4: %d \n", num3, num4);
}
 

//=============================================
// 덧셈 프로그램의 완성 (P.8)
//=============================================
void addTwo(void)
{
	int num1=3;
	int num2=4;
	// 변수의 선언과 동시에 덧셈 연산의 결과로 초기화 한다.
	int result=num1+num2;

	printf("덧셈 결과: %d \n", result);
	printf("%d+%d=%d \n", num1, num2, result);
	printf("%d와(과) %d?의 합은 %d입니다.\n", num1, num2, result);
}


//=============================================
// 대입 연산자와 산술 연산자  (P.10)
//=============================================
void operatorOne(void)
{
	// + - * /
	int num1=9, num2=2;
	printf("%d＋%d＝%d \n", num1, num2, num1+num2);
	printf("%d－%d＝%d \n", num1, num2, num1-num2);
	printf("%d×%d＝%d \n", num1, num2, num1*num2);
	printf("%d÷%d의 몫＝%d \n", num1, num2, num1/num2);
	printf("%d÷%d의 나머지＝%d \n", num1, num2, num1%num2);
}


//=============================================
// 복합 대입 연산자 (P.11)
//=============================================
void operatorTwo(void)
{
	int num1=2, num2=4, num3=6;
	num1 += 3;	// num1 = num1 + 3;
	num2 *= 4;	// num2 = num2 * 4;
	num3 %= 5;	// num3 = num3 % 5;

	printf("Result: %d, %d, %d \n", num1, num2, num3);
}


//=============================================
// 부호의 의미를 갖는 +연산자 -연산자  (P.12) 
//=============================================
void operatorThree(void)
{
	int num1 = +2;
	int num2 = -4;

	num1 = -num1;
	printf("num1: %d \n", num1);
	num2 =- num2;
	printf("num2: %d \n", num2);
}


//=============================================
// 증가, 감소연산자 (P.13) 
//=============================================
void operatorFour(void)
{
	int num1=12;
	int num2=12;

	printf("num1: %d \n", num1);
	printf("num1++: %d \n", num1++);
	printf("num1: %d \n\n", num1);

	printf("num2: %d \n", num2);
	printf("++num2: %d \n", ++num2);
	printf("num2: %d \n", num2);

}


//=============================================
// 증가, 감소 연산자 추가 예제(P.14) 
//=============================================
void operatorFive(void)
{
	int num1=10;
	int num2=(num1--)+2;
	
	printf("num1: %d \n", num1);
	printf("num2: %d \n", num2);

}


//=============================================
// 관계 연산자 (P.15) 
//=============================================
void operatorSix(void)
{
	int num1=10;
	int num2=12;
	int result1, result2, result3;

	result1=(num1==num2);
	result2=(num1<=num2);
	result3=(num1>num2);
	
	printf("result1: %d \n", result1);
	printf("result2: %d \n", result2);
	printf("result3: %d \n", result3);

}


//=============================================
// 논리 연산자 (P.16) 
//=============================================
void operatorSeven(void)
{
	int num1=10;
	int num2=12;
	int result1, result2, result3;
	
	result1 = (num1==10 && num2==12);
	result2 = (num1<12 || num2>12);
	result3 = (!num1);
	
	printf("result1: %d \n", result1);
	printf("result2: %d \n", result2);
	printf("result3: %d \n", result3);

}

//=============================================
// 콤마 연산자 (P.17) 
//=============================================
void commaOp(void)
{
	int num1=1, num2=2;
	printf("Hello "), printf("world! \n");
	num1++, num2++;
	printf("%d ", num1), printf("%d ", num2), printf("\n");

}

//=============================================
// 키보드로부터의 정수입력을 위한 scanf 함수의 호출 (P.20) 
//=============================================
void addThree(void)
{
	int result;
	int num1, num2;

	printf("정수 one: ");
	scanf("%d", &num1);	    // 첫 번째 정수 입력
	printf("정수 two: ");
	scanf("%d", &num2);	    // 두 번째 정수 입력
	
	result=num1+num2;
	printf("%d + %d = %d \n", num1, num2, result);

}
