#include <iostream>

using namespace std;

template <typename T>
T TestFunc(T a)
{
	std::cout << "매개변수 a : " << a <<endl;

	return a;
}

int main(int argc, char** argv)
{
	cout << "int\t"<< TestFunc(3) <<endl;
	cout << "double\t" << TestFunc(3.3) <<endl;
	cout << "char\t"<< TestFunc('A') <<endl;
	cout << "char*\t"<< TestFunc("Teststring") <<endl;

	retur 0;
}
