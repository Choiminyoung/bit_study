#include <iostream>

int TestFunc(int nParam=10)
{
	return nParam;
}

int main(int argc, char** argv)
{
	// 호출자가 실인수를 기술하지 않았으므로 디폴트값을 적용
	std::cout << TestFunc() << std::endl;
	
	// 호출자가 실인수를 확정했으므로 디폴트값을 무시
	std::cout << TestFunc(20) << std::endl;

	return 0;
}
