/*
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#define NUMBER_1 	10000


void* thread_function(void)
{
	char *a = malloc(10);
	printf("a의 주소 : %p\n",a);

	strcpy(a,"hello world");
	pthread_exit((void*)a);
}
int main()
{
	pthread_t thread_id;
	char *b;

	pthread_create (&thread_id, NULL,&thread_function, NULL);

	pthread_join(thread_id,(void**)&b); //here we are reciving one pointer value so to use that we need double pointer 
	printf("b is %s\n",b); 
	printf("b의 주소 : %p\n",b);
	free(b); // lets free the memory
}
*/


/*
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
 
// 쓰레드 함수
void *t_function(void *data)
{
	pid_t pid;            // process id
	pthread_t tid;        // thread id

	pid = getpid();
	tid = pthread_self();

	char* thread_name = (char*)data;
	int i = 0;

	while (i<3)   // 0,1,2 까지만 loop 돌립니다.
	{
		// 넘겨받은 쓰레드 이름과 
		// 현재 process id 와 thread id 를 함께 출력
		printf("[%s] pid:%u, tid:%x --- %d\n", 
		thread_name, (unsigned int)pid, (unsigned int)tid, i);
		i++;
		sleep(1);  // 1초간 대기
	}
}

int main()
{
	pthread_t p_thread[2];

	int thr_id;
	int status;
	char p1[] = "thread_1";   // 1번 쓰레드 이름
	char p2[] = "thread_2";   // 2번 쓰레드 이름
	char pM[] = "thread_m";   // 메인 쓰레드 이름


	sleep(1);  // 2초 대기후 쓰레드 생성

	// ① 1번 쓰레드 생성
	// 쓰레드 생성시 함수는 t_function
	// t_function 의 매개변수로 p1 을 넘긴다.  
	thr_id = pthread_create(&p_thread[0], NULL, t_function, (void *)p1);

	// pthread_create() 으로 성공적으로 쓰레드가 생성되면 0 이 리턴됩니다
	if (thr_id < 0)
	{
		perror("thread create error : ");
		exit(0);
	}

	// ② 2번 쓰레드 생성
	thr_id = pthread_create(&p_thread[1], NULL, t_function, (void *)p2);

	if (thr_id < 0)
	{
		perror("thread create error : ");
		exit(0);
	}

	// ③ main() 함수에서도 쓰레드에서 돌아가고 있는 동일한 함수 실행
	t_function((void *)pM);

	// 쓰레드 종료를 기다린다. 
	pthread_join(p_thread[0], (void **)&status);
	pthread_join(p_thread[1], (void **)&status);
			 
	printf("언제 종료 될까요?\n");

	return 0;
}
*/



#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#define DBUG 	0
#define VAL_100000 		 	100000
#define VAL_200000 			200000
#define VAL_300000 			300000
#define VAL_400000 			400000
#define VAL_500000 			500000
#define VAL_600000 			600000
#define VAL_700000 			700000
#define VAL_800000 			800000
#define VAL_900000 			900000
#define VAL_1000000 		1000000

void *tFun1(void* arg){
	int i = 0;
	int num[10] = {0,};
	int int_test = 0;
	int case_val = (*(int*)arg);

	switch(case_val){
		case 0:
			for(i = 1; i <= VAL_100000; i++){
				num[case_val] += i; 

				if(num[case_val] < 0){
					printf("case 0 -값 : %d\n",num[case_val]);
					break;
				}
			} 
			printf("0 result %d\n",num[case_val]);
			break;
		case 1:
			for(i = VAL_100000+1; i <= VAL_200000; i++){
				num[case_val] += i; 

				if(num[case_val] < 0){
					printf("case 1 -값 : %d\n",num[case_val]);
					break;
				}
			}
			printf("1 result %d\n",num[case_val]);
			break;
		case 2:
			i = VAL_200000+1;
			printf("i : %d\n",i);
			for(; i <= VAL_300000; i++){
				num[case_val] += i; 
				if(num[case_val] < 0){
					printf("case 2 -값 : %d\n",num[case_val]);
					break;
				}
			for(i = VAL_200000+1; i <= VAL_300000; i++){
				num[case_val] += i; 
			}
			printf("2 result %d\n",num[case_val]);
			break;
		case 3:
			for(i = VAL_300000+1; i <= VAL_400000; i++){
				num[case_val]+= i; 
				if(num[case_val] < 0){
					printf("case 3 -값 : %d\n",num[case_val]);
					break;
				}
			}
			printf("3 result %d\n",num[case_val]);
			break;
		case 4:
			for(i = VAL_400000+1; i <= VAL_500000; i++){
				num[case_val] += i; 
				if(num[case_val] < 0){
					printf("case 4 -값 : %d\n",num[case_val]);
					break;
				}
			}
			printf("4 result %d\n",num[case_val]);
			break;
		case 5:
			for(i = VAL_500000+1; i <= VAL_600000; i++){
				num[case_val] += i; 
				if(num[case_val] < 0){
					printf("case 5 -값 : %d\n",num[case_val]);
					break;
				}
			}
			printf("5 result %d\n",num[case_val]);
			break;
		default:
			printf("\ndefault : %d\t",case_val);
			perror("thread create Error");
			break;
	}
 	
	if(DBUG){
		printf("thread => main에서 전달받은 주소 : %p\n",arg);
		printf("thread => thread &주소 : %p\n",&int_test);
	}
	return NULL;
}

int main(int argc, char** argv)
{
	pthread_t thread_id[6];

	int thread_result[6];
	int i;
	int* main_malloc;

	main_malloc = (int *)malloc(6 * sizeof(int));

	if(DBUG){
		printf("main malloc &주소 : %p\n",&main_malloc);
		printf("포이터 변수 main malloc 저장 값(주소) : %p\n",main_malloc);
		printf("main malloc 저장 값(주소)에 있는 값 : %d\n",*main_malloc);
	}

	// ===================================================
	// thread 생성
	// ===================================================
	for(i = 0; i < 5; i++){
		main_malloc[i] = i;
		printf("\nmain_malloc : %d\n ",main_malloc[i]);
		printf("\n&main_malloc : %p\n",(void*)&main_malloc[i]);
		printf("\nmain_malloc : %d \n ",main_malloc[i]);
		printf("\n&main_malloc : %p  \n",(void*)&main_malloc[i]);
		thread_result[i] = pthread_create(&thread_id[i], NULL, tFun1, (void*)(&(main_malloc[i])));

		// ===================================================
		// thread 생성 결과 확인 0보다 작을 경우 thread 생성 실패
		// ===================================================
		if(thread_result[i] < 0){
			printf("fail : %d\t",i);
			perror("thread create fail");
			return -1;
		}
	}

	if(DBUG){
		printf("main thread result : %d\n",*main_malloc);
		printf("main malloc &주소 : %p\n",main_malloc);
	}
	
	//here we are reciving one pointer value so to use that we need double pointer 
	
	for(i=0; i<5; i++){
		pthread_join(thread_id[i], NULL); 
	}
	printf("result : %d\n", *main_malloc);

	return 0;
}
