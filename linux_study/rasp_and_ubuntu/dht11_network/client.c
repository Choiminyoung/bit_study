#include <stdio.h>
#include <sys/time.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <time.h>
#include <fcntl.h>
#include <pthread.h>

#define BUF_SIZE 1024
#define TIME_QUANTUM 300000

int time_flag=0;
int thread_end_flag=0;
void * t_function(void * data);

struct strinfo {
	char temp;
	char freq;
	char time;
	int mday;
};

void error_handling(char *message);

int main(int argc, char *argv[])
{
	int sock;
	int str_len;
	struct sockaddr_in serv_adr;
	char message[20];
	FILE *tempfd, *freqfd;

	int thr_id;
	pthread_t p_thread;
	char buf[BUFSIZ];
	int fd=-1;
	int temp[100]={0,};
	int humi[100]={0,};
	int data[6]={0,};
	int max_tk=0;
	int max_hk=0;
	int final_temp=0;
	int final_humi=0;
	char * token;
	char str_buf[30];
	memset(buf,0,BUFSIZ);

	//send DATA
	struct strinfo INFO;

	//time
	time_t tim;
	struct tm *tm;
	time(&tim);
	tm = localtime(&tim);

	// exgamination arg
	if (argc != 3) {
		printf("Usage : %s <IP> <port>\n", argv[0]);
		exit(1);
	}

	// make socket
	sock = socket(PF_INET, SOCK_STREAM, 0);
	if (sock == -1)
		error_handling("socket() error");

	memset(&serv_adr, 0, sizeof(serv_adr));
	serv_adr.sin_family = AF_INET;
	serv_adr.sin_addr.s_addr = inet_addr(argv[1]);
	serv_adr.sin_port = htons(atoi(argv[2]));

	// connext
	if (connect(sock, (struct sockaddr*)&serv_adr, sizeof(serv_adr)) == -1)
		error_handling("connect() error!");
	else
		puts("Connected...........");

	fd=open("/dev/gpio_dht11",O_RDWR);
	if(fd==-1)
		printf("fd:-1\n");

	thr_id=pthread_create(&p_thread,NULL,t_function,NULL);

	for(int j=0;j<100;j++){

		if(time_flag==1 && data[0]==1){
			for(int k=0;k<100;k++){
				if(temp[k]>max_tk){
					max_tk=temp[k];
					final_temp=k;
				}
				if(humi[k]>max_hk){
					max_hk=temp[k];
					final_humi=k;
				}
			}
			INFO.temp=final_temp;
			INFO.freq=final_humi;
			INFO.time=1;
			write(sock,&INFO,sizeof(INFO));
				/*
					to server transport
				   */
			time_flag=0;
			memset(temp,0,sizeof(int)*100);
			memset(humi,0,sizeof(int)*100);
			max_tk=0;
			max_hk=0;
			final_temp=0;
			final_humi=0;
		}

		memset(buf,0,sizeof(char)*BUFSIZ);

		read(fd,buf,40);
		printf("Read data: %s",buf);
		usleep(TIME_QUANTUM);

		token=strtok(buf,",");
		int l=0;
		while(token!=NULL)
		{
			data[l]=atoi(token);
			l++;
			token=strtok(NULL,",");
		}
	/*	for(int m=0;m<6;m++)
			printf("data[%d]:%d\n",m,data[m]);
*/

		if(data[0]==1){
			temp[data[1]]++;
			humi[data[3]]++;
		}
		else{
			continue;
		}
	}
	thread_end_flag=1;
	pthread_detach(p_thread);

	close(fd);
	close(sock);
	return 0;
}

void error_handling(char *message)
{
	fputs(message, stderr);
	fputc('\n', stderr);
	exit(1);
}

void * t_function(void *data){
	while(1){
		sleep(5);
		time_flag=1;
		if(thread_end_flag==1)
			break;
	}
	return NULL;
}

