#include <stdio.h>
#include <sys/time.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <time.h>

#define BUF_SIZE 1024

struct strinfo  {
    char temp[10];
    char freq[10];
    char time[30];
    int mday;
};

void error_handling(char *message);

int main(int argc, char *argv[])
{
    int sock;
    int str_len;
    struct sockaddr_in serv_adr;
    char message[20];
    FILE *tempfd, *freqfd;

    //send DATA
    struct strinfo INFO;

    //time
    time_t tim;
    struct tm *tm;
    time(&tim);
    tm = localtime(&tim);

    // exgamination arg
    if(argc!=3) {
        printf("Usage : %s <IP> <port>\n", argv[0]);
        exit(1);
    }

    // make socket
    sock=socket(PF_INET, SOCK_STREAM, 0);
    if(sock==-1)
        error_handling("socket() error");

    memset(&serv_adr, 0, sizeof(serv_adr));
    serv_adr.sin_family=AF_INET;
    serv_adr.sin_addr.s_addr=inet_addr(argv[1]);
    serv_adr.sin_port=htons(atoi(argv[2]));

    // connext
    if(connect(sock, (struct sockaddr*)&serv_adr, sizeof(serv_adr))==-1)
        error_handling("connect() error!");
    else
        puts("Connected...........");

    while(1)
    {
        // get temp
        tempfd = fopen("/sys/class/thermal/thermal_zone0/temp", "r");
        fgets(INFO.temp,sizeof(INFO.temp), tempfd);
        printf("temp: %s \n",INFO.temp);
        fclose(tempfd);
        // ger freq
        freqfd = fopen("/sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq", "r");
        fgets(INFO.freq,sizeof(INFO.freq), freqfd);
        printf("freq: %s \n",INFO.freq);
        fclose(freqfd);

        // get time
        sprintf(INFO.time,"time: %2d:%2d:%2d\n", tm->tm_hour,tm->tm_min,tm->tm_sec);
        printf("%s\n",INFO.time);

        // get mday
        INFO.mday = tm->tm_mday;
        printf("day: %d\n",INFO.mday);

        //delebery information
        write(sock, &INFO, sizeof(INFO));

        //complit message
        str_len=read(sock, message, sizeof(message));
        message[str_len]=0;
        printf("%s \n", message);
        sleep(3);
    }

    close(sock);
    return 0;
}

void error_handling(char *message)
{
    fputs(message, stderr);
    fputc('\n', stderr);
    exit(1);
}

