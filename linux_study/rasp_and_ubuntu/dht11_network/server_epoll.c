#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include <sys/time.h>

#define BUF_SIZE 100
#define EPOLL_SIZE 50

struct strinfo  {
    char temp;
    char freq;
    char time;
    int mday;
};

void error_handling(char *buf);
void logFILE(char *temp, char *freq, char *time, int *mday);

int main(int argc, char *argv[])
{
    int serv_sock, clnt_sock;
    struct sockaddr_in serv_adr, clnt_adr;
    socklen_t adr_sz;
    int str_len, i;
    char buf[BUF_SIZE];

    //getmessage
    char getmessage[20];
    strcpy(getmessage, "message complet!");

    struct epoll_event *ep_events;
    struct epoll_event event;
    int epfd, event_cnt;

    //send DATA
    struct strinfo INFO;

    if(argc!=2) {
        printf("Usage : %s <port>\n", argv[0]);
        exit(1);
    }

    serv_sock=socket(PF_INET, SOCK_STREAM, 0);
    memset(&serv_adr, 0, sizeof(serv_adr));
    serv_adr.sin_family=AF_INET;
    serv_adr.sin_addr.s_addr=htonl(INADDR_ANY);
    serv_adr.sin_port=htons(atoi(argv[1]));

    if(bind(serv_sock, (struct sockaddr*) &serv_adr, sizeof(serv_adr))==-1)
        error_handling("bind() error");
    if(listen(serv_sock, 2)==-1)
        error_handling("listen() error");

    epfd=epoll_create(EPOLL_SIZE);
    ep_events=malloc(sizeof(struct epoll_event)*EPOLL_SIZE);

    event.events=EPOLLIN;
    event.data.fd=serv_sock;
    epoll_ctl(epfd, EPOLL_CTL_ADD, serv_sock, &event);


    while(1)
    {
        event_cnt=epoll_wait(epfd, ep_events, EPOLL_SIZE, -1);
        if(event_cnt==-1)
        {
            puts("epoll_wait() error\n");
            break;
        }

        for(i=0; i<event_cnt; i++)
        {
            if(ep_events[i].data.fd==serv_sock)
            {
                adr_sz=sizeof(clnt_adr);
                clnt_sock=
                    accept(serv_sock, (struct sockaddr*)&clnt_adr, &adr_sz);
                event.events=EPOLLIN;
                event.data.fd=clnt_sock;
                epoll_ctl(epfd, EPOLL_CTL_ADD, clnt_sock, &event);
                printf("connected client: %d \n", clnt_sock);
            }
            else
            {
//                    str_len=read(ep_events[i].data.fd, buf, BUF_SIZE);
                    str_len=read(ep_events[i].data.fd,&INFO,sizeof(INFO));

						if(str_len==0)    // close request!
						{
							epoll_ctl(
								epfd, EPOLL_CTL_DEL, ep_events[i].data.fd, NULL);
							shutdown(ep_events[i].data.fd, SHUT_RDWR);
							close(ep_events[i].data.fd);
							printf("closed client: %d \n", ep_events[i].data.fd);
						}
						else
						{
							printf("%s\n",getmessage);
							write(ep_events[i].data.fd, getmessage, sizeof(getmessage));
							//FILE func
							logFILE(&INFO.temp, &INFO.freq, &INFO.time, &INFO.mday);
	/*                      printf("temp: %s \n", INFO.temp);
							printf("freq: %s \n", INFO.freq);
							printf("%s \n", INFO.time);
							printf("day: %d \n", INFO.mday); */

						}

				}
			}
		}
		close(serv_sock);
		close(epfd);
		return 0;
	}

	void logFILE(char *Temp, char *Humidity, char *UserName, int *mday)
	{
		FILE *fp;
		char filename[20];
		char logtext[20];
		//serv time
		time_t UTCtim;
		struct tm *ptm;  //= malloc(sizeof(struct tm));
		int hour, name; //file name & user no. 

		time(&UTCtim);
		ptm = localtime(&UTCtim);
		sprintf(filename,"%d-%2d-%2d-%2d:user%d.txt",ptm->tm_year+1900, ptm->tm_mon, ptm->tm_mday, ptm->tm_hour, *UserName);
		printf("Server time: %2d:%2d:%2d - user %d\n", ptm->tm_hour,ptm->tm_min,ptm->tm_sec,*UserName);


		fp = fopen(filename,"at+");
		if(fp==NULL)    {
			printf("error");
			exit(1);
		}
		fprintf(fp,"Server time: %2d:%2d\n", ptm->tm_min,ptm->tm_sec);
		fprintf(fp,"tmep: %2d, humidity: %2d\n", *Temp,*Humidity);
	//    fprintf(fp,"Client %stemp: %sfreq: %s",ctime,temp,freq);
		fclose(fp);
    //free(ptm);
    return ;
}


void error_handling(char *buf)
{
    fputs(buf, stderr);
    fputc('\n', stderr);
    exit(1);
}
