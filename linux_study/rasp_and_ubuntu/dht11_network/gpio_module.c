#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/module.h>
#include <linux/io.h>
#include <asm/uaccess.h>

#include <linux/errno.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/kernel.h>
#include <linux/time.h>
#include <linux/string.h>
#include <linux/types.h>
#include <linux/wait.h>
#include <linux/mm.h>
#include <linux/delay.h>

#include <linux/syscore_ops.h>
#include <linux/irq.h>
#include <linux/fcntl.h>
#include <linux/spinlock.h>
#include <linux/gpio.h>
#include <linux/sizes.h>
#include <linux/device.h>


MODULE_LICENSE("GPL");
MODULE_AUTHOR("YoungJin Suh");
MODULE_DESCRIPTION("Raspberry Pi GPIO LED Device Module");

#define BCM_IO_BASE 0x3F000000
#define GPIO_BASE (BCM_IO_BASE + 0x200000)
#define GPIO_SIZE 0xB4

#define GPIO_IN(g)  (*(gpio+((g)/10)) &= ~(7<<(((g)%10)*3)))
#define GPIO_OUT(g) (*(gpio+((g)/10)) |= (1<<(((g)%10)*3)))

#define GPIO_SET(g) *(gpio+7) = 1<<g;
#define GPIO_CLR(g) *(gpio+10) = 1<<g;
#define GPIO_GET(g) (*(gpio+13)&(1<<(g))) && 1

#define GPIO_INT_CLEAR(g) *(gpio+16) = (*(gpio+16) | (1<<g));

#define GPIO_INT_RISING(g,v)	*(gpio+19) = v ? (*(gpio+19) | (1<<g)) : (*(gpio+19) ^ (1<<g))
#define GPIO_INT_FALLING(g,v)	*(gpio+22) = v ? (*(gpio+22) | (1<<g)) : (*(gpio+22) ^ (1<<g))

	

#define GPIO_MAJOR 200
#define GPIO_MINOR 0
#define GPIO_DEVICE "dht11"
#define GPIO_PIN1 18

static int sense=0;
static struct timeval lasttv = {0,0};

volatile unsigned *gpio;
static char msg[BLOCK_SIZE]={0};

static int gpio_open(struct inode *, struct file *);
static ssize_t gpio_read(struct file *, char *, size_t, loff_t *);
static ssize_t gpio_write(struct file *,const char *,size_t,loff_t *);
static int gpio_close(struct inode *,struct file *);

static irqreturn_t irq_handler(int i,void *blah, struct pt_regs *regs);
static int setup_interrupts(void);
static void clear_interrupts(void);


static int Device_Open=0;
static spinlock_t lock;
static unsigned int bitcount=0;
static unsigned int bytecount=0;
static unsigned int started=0;

static unsigned int dht[5];

static int INTERRUPT_GPIO0;

static struct file_operations gpio_fops={
	.owner = THIS_MODULE,
	.read = gpio_read,
	.write = gpio_write,
	.open = gpio_open,
	.release = gpio_close,
};

struct cdev gpio_cdev;

static irqreturn_t irq_handler(int i,void *blah, struct pt_regs *regs){
	struct timeval tv;
	long deltv;
	int data=0;
	int signal;

	signal = GPIO_GET(GPIO_PIN1);

	GPIO_INT_CLEAR(GPIO_PIN1);

	if(sense !=-1){
		do_gettimeofday(&tv);

		deltv=tv.tv_sec-lasttv.tv_sec;

		data=(int) (deltv*1000000+(tv.tv_usec - lasttv.tv_usec));

		lasttv=tv;

		if((signal == 1) &&(data>40)){
			started=1;
			return IRQ_HANDLED;
		}

		if((signal == 0)&&(started ==1)){
			if(data>80)
				return IRQ_HANDLED;
			if(data<15)
				return IRQ_HANDLED;
			if(data>60)
				if(bytecount<5)
				dht[bytecount]=dht[bytecount] | (0x80 >> bitcount);
			bitcount++;
			if(bitcount==8){
				bitcount=0;
				bytecount++;
			}
		}
	}
	return IRQ_HANDLED;
}


int GPIO_init(void){
	dev_t devno;
	unsigned int count;
	static void *map;
	int err;

	printk(KERN_INFO "Hello module!\n");

	//try_module_get(THIS_MODULE);

	devno = MKDEV(GPIO_MAJOR,GPIO_MINOR);
	register_chrdev_region(devno,1,GPIO_DEVICE);

	cdev_init(&gpio_cdev,&gpio_fops);

	gpio_cdev.owner = THIS_MODULE;
	count =1;
	err= cdev_add(&gpio_cdev,devno,count);
	if(err<0){
		printk("Error : Device Add\n");
		return -1;
	}

	/*printk("'mknod /dev/%s c %d 0'\n",GPIO_DEVICE, GPIO_MAJOR);
	printk("'chmod 666 /dev/%s'\n",GPIO_DEVICE);*/

	gpio = ioremap_nocache(GPIO_BASE, SZ_4K);

	if(gpio==NULL){
		printk("Error : mapping GPIO memory\n");
		iounmap(map);
		return -EBUSY;
	}

	printk("'mknod /dev/%s c %d 0'\n",GPIO_DEVICE, GPIO_MAJOR);
	printk("'chmod 666 /dev/%s'\n",GPIO_DEVICE);


	return 0;
}

void GPIO_exit(void){
	dev_t devno = MKDEV(GPIO_MAJOR , GPIO_MINOR);
	unregister_chrdev_region(devno,1);

	cdev_del(&gpio_cdev);

	if(gpio){
		iounmap(gpio);
		release_mem_region(GPIO_BASE,SZ_4K);
	}


	printk(KERN_INFO "Good-bye module!\n");

}


static int gpio_open(struct inode * inod,struct file *fil){
	
	printk(KERN_INFO "GPIO Device opened(%d/%d)\n",imajor(inod),iminor(inod));
	if(Device_Open)
		return -EBUSY;
	
	try_module_get(THIS_MODULE);
	Device_Open++;

	return 0;
}

static int gpio_close(struct inode *inod,struct file *fil){
	printk("GPIO Device close(%d:%d)\n",imajor(inod),iminor(inod));
	module_put(THIS_MODULE);
	Device_Open--;
	return 0;
}

static int setup_interrupts(void){
	int result;
	unsigned long flags;

	INTERRUPT_GPIO0=gpio_to_irq(GPIO_PIN1);
	result = request_irq(INTERRUPT_GPIO0,(irq_handler_t)irq_handler,0,GPIO_DEVICE,(void*)gpio);

	
	switch(result){
		case -EBUSY:
			printk(KERN_ERR GPIO_DEVICE ": IRQ %d is busy\n",INTERRUPT_GPIO0);
			return -EBUSY;
		case -EINVAL:
			printk(KERN_ERR GPIO_DEVICE ": Bad irq number or handler\n");
			return -EINVAL;
		default:
			printk(KERN_INFO GPIO_DEVICE ": Interrupt %04x obtained\n",INTERRUPT_GPIO0);
			break;
	};

	spin_lock_irqsave(&lock,flags);

	GPIO_INT_RISING(GPIO_PIN1,1);
	GPIO_INT_FALLING(GPIO_PIN1,1);

	GPIO_INT_CLEAR(GPIO_PIN1);

	spin_unlock_irqrestore(&lock,flags);

	
	return 0;
}


static ssize_t gpio_read(struct file *inode, char *buff,size_t len,loff_t  *off){
	char str[256];
	unsigned char flag_bit=0;
	int bytes_read=0;
	char *msg_Ptr;

	started=0,bitcount=0,bytecount=0;
	dht[0]=0,dht[1]=0,dht[2]=0,dht[3]=0,dht[4]=0;
	GPIO_OUT(GPIO_PIN1);
	GPIO_CLR(GPIO_PIN1);
	mdelay(20);
	GPIO_SET(GPIO_PIN1);
	udelay(40);
	GPIO_IN(GPIO_PIN1);

	do_gettimeofday(&lasttv);

	setup_interrupts();

	mdelay(10);

	printk(KERN_INFO GPIO_DEVICE "dht[0]:%d, dht[1]:%d, dht[2]:%d, dht    [3]:%d, dht[4]:%d\n",dht[0],dht[1],dht[2],dht[3],dht[4]);

	if((dht[0]+dht[1]+dht[2]+dht[3]==dht[4]) & (dht[4]>0)){
		flag_bit=1;
	}
	
	else{
		flag_bit=0;
	}
	
	sprintf(str,"%d, %d, %d, %d, %d, %d\n",flag_bit,dht[0],dht[1],dht[2],dht[3],dht[4]);
	printk(KERN_INFO "%s",str);
	
	msg_Ptr=str;
	if(*msg_Ptr==0)return 0;

	//printk(KERN_INFO "%s",msg_Ptr);
	while(len && *msg_Ptr){
		put_user(*(msg_Ptr++),buff++);

		len--;
		bytes_read++;
	}

	clear_interrupts();
	return bytes_read;
}

static ssize_t gpio_write(struct file * inode, const char *buff,size_t len, loff_t *off){
	ssize_t count=0;
	printk("GPIO Device write : %s(%d)\n",msg,len);

	return count;
}

static void clear_interrupts(void){
	unsigned long flags;

	spin_lock_irqsave(&lock, flags);

	GPIO_INT_RISING(GPIO_PIN1,0);

	GPIO_INT_FALLING(GPIO_PIN1,0);

	spin_unlock_irqrestore(&lock,flags);

	free_irq(INTERRUPT_GPIO0,(void *)gpio);
}

module_init(GPIO_init);
module_exit(GPIO_exit);
