#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>

void error_handling(char *message);

int main(int argc, char** argv)
{
	int server_socket;
	int client_socket;

	struct sockaddr_in serv_addr;
	struct sockaddr_in clnt_addr;

	socklen_t clnt_addr_size;

	int step = 0, status = 0;

	if(argc != 2){
		printf("Usage : %s <port>\n",argv[0]);
		exit(1);
	}

	// ================================================
	// socket 생성
	// ================================================
	server_socket = socket(PF_INET, SOCK_STREAM, 0);
	if(server_socket == -1)
			error_handling("socket() error");

	memset(&serv_addr, 0, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr  = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(atoi(argv[1]));

	// ==================================================
	// bind 
	// ==================================================
	if(bind(server_socket, (struct sockaddr*) & serv_addr, sizeof(serv_addr)) == -1)
		error_handling("bind() error");
	if(listen(server_socket, 5) == -1)
		error_handling("listen() error");

	// ==================================================
	// accecpt
	// ==================================================
	clnt_addr_size = sizeof(clnt_addr);
	client_socket = accept(server_socket, (struct sockaddr*)&clnt_addr, &clnt_addr_size);
	if(client_socket == -1)
		error_handling("accept() error");


	while(step < 9){
		status = read(client_socket, &step, sizeof(step));
		if(status < 0){
			printf("step : %d Read error \n",step);
			exit(1);
		}
		else{
			printf("step : %d read ok\n",step);
		}

		step++;
		
		status = write(client_socket, &step, sizeof(step));
		if(status < 0){
			printf("step : %d write error\n",step);
			exit(1);
		} else{
			printf("step : %d write ok\n",step);
		}
	}
	close(client_socket);
	close(server_socket);

	return 0;
}

void error_handling(char* message)
{
	fputs(message, stderr);
	fputc('\n',stderr);
	exit(1);
}
