#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>

void error_handling(char* message);

int main(int argc ,char** argv)
{
	int client_socket;
	struct sockaddr_in serv_addr;
	int buf_len;
	int step = 0;
	int status = 0;

	if(argc != 3){
		printf("Usage : %s <IP> <PORT>\n",argv[0]);
		exit(1);
	}

	// ====================================================
	// socket 생성
	// ====================================================
	client_socket = socket(PF_INET, SOCK_STREAM, 0);
	if(client_socket == -1)
		error_handling("client_socket() error");

	// ====================================================
	// connect 
	// ====================================================
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(atoi(argv[2]));
	serv_addr.sin_addr.s_addr = inet_addr(argv[1]);

	if(connect(client_socket, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) == -1)
		error_handling("connect() error!");

	while(step < 10){
		status = write(client_socket, &step, sizeof(step));
		if(status < 0)
			printf("step :%d write() error!\n",step);
		else
			printf("step : %d write OK!\n",step);

		status = read(client_socket, &step, sizeof(step));
		if(status < 0)
			printf("step :%d read() error!\n",step);
		else
			printf("step : %d read OK!\n",step);

		step++;
	}

	close(client_socket);
	return 0;
}

void error_handling(char* message)
{
	fputs(message, stderr);
	fputc('\n',stderr);
	exit(1);
}
