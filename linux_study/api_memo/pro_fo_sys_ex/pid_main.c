#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

/* Remember the effective and real UIDs. */
static uid_t euid, ruid;


/* Restore the effective UID to its original value. */
void  do_setuid (void)
{
	int status;
		 
#ifdef _POSIX_SAVED_IDS
	status = seteuid (euid);
#else
	status = setreuid (ruid, euid);
#endif
	if (status < 0) {
		fprintf (stderr, "Couldn't set uid.\n");
		exit (status);
	}

}


/* Set the effective UID to the real UID. */
void  undo_setuid (void)
{
	int status;
		 
#ifdef _POSIX_SAVED_IDS
	status = seteuid (ruid);
#else
	status = setreuid (euid, ruid);
#endif
	if (status < 0) {
		fprintf (stderr, "Couldn't set uid.\n");
		exit (status);
	}
}


int record_score (int score)
{
FILE *stream;
char *myname;
/* Open the scores file. */
do_setuid ();
stream = fopen (SCORES_FILE, "a");
undo_setuid ();




 /* Main program. */
     
int main (void)
{
	/* Remember the real and effective user IDs.  */
	ruid = getuid ();
	euid = geteuid ();
	undo_setuid ();

	/* Do the game and record the score.  */
	 //...
	/* Write the score to the file. */
	if (stream)
	{
		myname = cuserid (NULL);
		if (score < 0)
			fprintf (stream, "%10s: Couldn't lift the caber.\n", myname);
		else
			fprintf (stream, "%10s: %d feet.\n", myname, score);
			fclose (stream);
			return 0;
		}
		else
			return -1;
	}
}


