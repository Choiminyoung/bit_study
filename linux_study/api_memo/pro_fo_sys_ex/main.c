#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

//int global =10;

//int glob = 6;
//char buf[] = "a write to stdout\n";

//static int g_val = 1;
//char str[] = "PID";

void pr_exit(int status);

int main(void)
{
	pid_t pid;
	char* message;
	int n;

	printf("fork program starting \n");
	pid = fork();


	switch(pid)
	{
		case -1:
			perror("fork failed");
			exit(1);
		case 0:
			message = "This is the child ";
			n=3;
			break;
			
	}

	for(;n > 0; n--){
		puts(message);
		sleep(1);
	}
	exit(0);
	return 0;
}


void pr_exit(int status)
{
	if(WIFEXITED(status))
		printf("normal termination, exit status = %d\n",
				WEXITSTATUS(status));
	else if(WIFSIGNALED(status))
		printf("abnormal termiantion, signal number = %d%s\n", 
				WTERMSIG(status),
				WCOREDUMP(status) ? "(core file generated)" : "" );
	else if(WIFSTOPPED(status))
		printf("child stopped, signal number = %d\n", 
				WSTOPSIG(status));
}

/*
 *
	pid_t pid;
	int status;

	if((pid = fork())<0)	perror("fork error");
	else if(pid == 0)	exit(7);

	if(wait(&status) == pid)	// wait for child
		pr_exit(status);		// and print its status
	
	if((pid = fork()) < 0)	perror("fork error ");	
	else if(pid == 0)	abort();	// child : generates SIGBRT

	if(wait(&status) == pid)	// wait for child 
		pr_exit(status);		// and print its status

	if((pid = fork()) < 0)	perror("frok error");
	else if(pid == 0) status /= 0;	// child: dibide bt 0 generates

	if(wait(&status) == pid)	// wait for child
		pr_exit(status);		// and print its status
	
 * */

/*
 *
	printf("fork start");
	pid = fork();

	switch(pid)
	{
		case -1:
			perror("fork failed");
			exit(1);
		case 0:
			message = "This is the child";
			n = 5; exit_code = 37;
			break;
		default:
			message = "This is the parent";
			n = 3; exit_code = 0;
			break;
	}

	for(; n<0; n--){
		puts("message");
		sleep(1);
	}


	if(pid != 0){
		int stat_val;
		pid_t chilid_pid;
		printf("before\n");

		chilid_pid = wait(&stat_val);
		printf("Child has finsished : PID = %d\n",chilid_pid);

		if(WIFEXITED(stat_val))
			printf("child exited with code %d\n", WEXITSTATUS(stat_val));
		else 
			printf("Child teminated abnormally\n");
			
	}
	exit(exit_code);
		

 * */


/*
 *
	int var;
	pid_t pid;
	var = 88;

	if((pid = vfork()) < 0)
	{
		perror("vfork");
	}
	else if(pid == 0)
	{
		g_val++;
		var++;
		printf("Parent %s form Child Process(%d) : %d\n",
				str, getpid(),getppid());
		_exit(0);
	} else { //부모 프로세스
		printf("Child %s from Parent Process(%d) : %d \n",str,getpid(),pid());

	}

	printf("");
 * */
/*
	pid_t pid;
	char * message;
	int n;

	printf("fork program starting\n");
	pid = fork();

	switch(pid)
	{
		case -1:
			perror("fork failed");
			exit(1);
		case 0:
			message = "This is the child";
			n = 5;
			break;
		default:
			message = "This is the parent";
			n = 3;
			break;
	}

	for(; n > 0; n--)
	{
		puts(message);
		sleep(1);
	}

	exit(0);

	*/
	/*
	int x;

	x = 0;

	fork();

	x = 1;

	printf("I am process %ld nad my x is %d\n", (long)getpid(),x);

	*/



/*
	int i =20; 
	pid_t pid;
	int status;

	if((pid=fork())==0)
	{ // child process 
		global = global + 10;
		i += 10;
	}
	else {
		// fork() return a pid !=0 
		// parent process 

		global = global + 100;
		i += 100;
	}

	printf("global = %d; i = %d\n",global,i);
	*/

/*
	int var;	// automatic variable on the stack
	pid_t pid;
	var = 88;

	if(write(STDOUT_FILENO, buf, sizeof(buf)-1) != sizeof(buf)-1)
		perror("write error");

	printf("before fork\n");

	if((pid=fork()) == 0)
	{	// child
		glob++;
		var++;
		printf("cpid = %d, glob = %d , var = %d \n",getpid(), glob, var);	
	} else { // parent
		sleep(2);
		printf("ppid = %d, glob = %d , var = %d \n",getpid(), glob, var);	
	}

	//printf("pid = %d, glob = %d , var = %d \n",getpid(), glob, var);	
	exit(0);
	*/
