#include <stdio.h>
#include <ctype.h>

int maina(int argc, char** argv)
{
	int ch;
	char* filename;
	if(argc != 2)
	{
		fprintf(stderr,"usage : useupper file \n");
		exit(1);
	}
	filename = argv[1];

	if(!freopen(filename,"r",stdin)){
		fprintf(stderr,"could not redirect stdin form file %s \n",filename);
		exit(2);
	}

	exed("./upper","upper",0);
	perror("could not exec ./upper");
	exit(3);
}

