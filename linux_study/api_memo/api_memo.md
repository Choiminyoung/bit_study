# Linux System Programming
# [참고 링크](http://mintnlatte.tistory.com/285)
## 강의때 접했던 API 
### List
* open() : 파일을 읽기 / 쓰기 위해 연다.  
    * 파일이 없으면 생성 가능*
* creat() : 빈 파일을 생성한다 . 
    * open() 함수에서 가능
* close() : 열려 있는 파일을 닫는다 . read() 파일로부터 데이터를 읽어온다 . 
    * 파일을 Open 한 상태에서
* write() : 파일에 데이터를 저장한다 . 
    *  파일을 Open 한 상태에서
* lseek() : 파일 포인터를 특정 위치로 이동시킨다 . unlink() 파일을 삭제한다 . 
* remove() : 파일이나 디렉터리를 삭제한다 . 
* fcntl() 파일과 관련되어 있는 속성을 설정하거나 조정한다 . 
* dup() : 파일 디스크립터를 복사한다 .

***

## API의 상세 설명 정리
### open()
* 함수 원형: 
    * int open(const char* path, int flags);
    * int open(const char* path, int flags, mode_t mode);
* path :
    * 파일의 경로를 포함한 이름을 나타내는 문자열의 포인터
* flags :
    * 파일 오픈 모드
    * flags에 인자로 넘겨줄수 있는 값과 그의미는다음과 같으며 하나이상의 모드를 OR(|)연산으로 묶어서 전달 가능하다.
        * O_CREAT : 필요한 경우 파일을 생성
        * O_TRUNC : 존재하던 데이터를 모두 삭제한다.
        * O_APPEND : 존재하던 데이터를 보존하고 뒤에 이어서 저장한다.
        * O_RDONLY : 읽기 전용 모드로 파일을 연다
        * O_WRONLY : 쓰기 전용 모드로 파일을 연다
        * O_RDWR : 읽기, 쓰기 겸용모드로 파일을 연다.
* mode :
    * 생성파일의 권한 설정
* 리턴 값:
    * 성공시 : 파일 디스크립터
    * 실패시 : -1

* exmple)
>
<pre>
    <code>
    int fd;
    fd = open(“/etc/passwd”, O_RDONLY);
    fd = open(“bit”, O_RDWR | O_APPEND);
    fd = open(“bit”, O_RDWR | O_CREAT | O_EXCL, 0644);
    </code>
</pre>

### 접했던 api가 사용되었던 곳
#### process 
* 
#### signal


sigaddset(&newmask, SIGINT) -> newmask에 sigint값을 추가하려면

* sigempty
* sigaddset
* sigprocmask