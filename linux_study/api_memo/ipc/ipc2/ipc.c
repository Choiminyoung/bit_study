#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

int main()
{
	int n, i = 0, status = 0,
		 pi_p[2];
	char buffer[BUFSIZ]="";
	pid_t pid;

	if(pipe(pi_p) < 0)
	{
		// int pipe(int filedes[2]); //형태
		// pipe -> return : 0 (성공), : -1 (실패)
		perror("pipe error : ");
		exit(1);
	}
	
	pid = fork(); 	// 자식프로세스 생성

	switch(pid)
	{
		case -1:	// 자식프로세스 생성 실패
			fprintf(stderr, "Fork failure");
			exit(EXIT_FAILURE);
			break;
		case 0: 	// 자식프로세스 수행중 때
			puts("pid : 0");
			printf("현재 프로세스 : %d \n",getpid());
			printf("부모 프로세스 : %d \n",getppid());
			sleep(3);

			break;
		default:	// 부모프로세스 실행 중일때
			puts("pid : default");
			printf("부모 프로세스 : %d \n",getpid());
			printf("자식 프로세스 : %d \n",getpid());
			sleep(3);


			break;
	}
	
	return 0;
}

