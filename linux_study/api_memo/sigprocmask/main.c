#include <stdio.h>
#include <signal.h>

static void sig_quit(int);

int main()
{
	sigset_t newmask ,oldmaks, pendmask;

	if(signal(SIGQUIT,sig_quit) == SIG_ERR)
		perror("can't catch SIGQUIT");
	
	sigemptyset(&newmask);
	sigaddset(&newmak, SIGQUIT);

	if(sigprocmask(SIG_BLOCK, &newmask, &oldmaks) < 0)
		perror("SIG_BLOCK error");

		sleep(5);
	if(sigpening(&pendmask) < 0)
		perror("sigpending error");
	if(sigismember(&pedmask, SIGQUIT))
		printf("\nSIGQUIT pending");

}
