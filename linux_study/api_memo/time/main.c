#include  <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <stdlib.h>

#define BUFSIZE 

int main(int argc, char** argv)
{
	time_t UTCtime;
	struct tm* tm;
	char buf[BUFSIZ];
	struct timeval UTCtime_u;

	time(&UTCtime); //UTC 현재시간
	printf("time : %u\n",(unsigned)UTCtime);
	printf("gettimeofday : %ld %d\n",UTCtime_u.tv_sec, UTCtime_u.tv_usec);

	printf("ctime :%s ", ctime(&UTCtime)); //현재의 시간을 무자열로 바꿔서 출력

	putenv("TZ = PST3PDT"); // 환경변수를 설정
	tzset(); //TZ변수를 설정
	tm = localtime(&UTCtime);	// tm = gmtime(&UTCtime);
	printf("asctime : %s", asctime(tm));

	strftime(buf,sizeof(buf), "%A %m %e %H:%M:%S: %Y",tm); //사용자 정의 문자열 지정
	printf("strftime : %s\n",buf);
}
