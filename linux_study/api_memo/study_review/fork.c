#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
	int pid = 0;

	pid = fork(); 	// fork()로 자식 프로세서 생성
	
	switch(pid){	// 프로세스들의 pid를 확인
		case -1:	// fork() 실패시
			printf("process fork fail");
			exit(pid);
			break;
		case 0:		// 자식 프로세스 일 때
			printf("pid == 0 일 때 (P)PID : %d  getpid() : %d \n",pid,getpid());
			break;
		default:	// 부모 프로세스 일 때
			printf("pid != 0 일 때 (C)PID : %d  getpid() : %d \n",pid,getpid());
			break;
	}
	return 0;
}

