#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>


static void sigHandler(int sig)
{
	static int count = 0;
	{
		count++;

		printf("Caught SIGINT (%d)\n", count);
		/* Resume execution at point of interruption */
		return;
	}
	/* Must be SIGQUIT - print a message and terminate the process */
	printf("Caught SIGQUIT - that's all folks!\n");
	exit(0);
}


int main(int argc, char *argv[])
{
	/* Establish same handler for SIGINT and SIGQUIT */
	if (signal(SIGINT, sigHandler) == SIG_ERR)
		exit("signal");
	if (signal(SIGQUIT, sigHandler) == SIG_ERR)
		exit("signal");
	for (;;)
		pause();
}
